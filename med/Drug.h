//
//  Drug.h
//  med
//
//  Created by Paul on 08/09/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Drug : NSManagedObject

@property (nonatomic) int drugClass;
@property (nonatomic, retain) NSString * indications;
@property (nonatomic, assign) BOOL isCore;
@property (nonatomic, retain) NSDate * lastEdited;
@property (nonatomic, retain) NSString * modeOfAction;
@property (nonatomic, retain) NSString * modeOfElimination;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * specialNotes;
@property (nonatomic, retain) NSString * typicalDose;
@property (nonatomic, retain) NSArray * specialInstructions;
@property (nonatomic, retain) NSArray * sideEffects;

@end
