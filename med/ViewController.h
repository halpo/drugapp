//
//  ViewController.h
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrugDetailViewController.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
	BOOL searching;
	int sections;
	MBProgressHUD *hud;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *drugArray;
@property (strong, nonatomic) NSArray *filteredDrugArray;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;

@end
