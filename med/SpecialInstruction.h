//
//  SpecialInstruction.h
//  med
//
//  Created by Paul on 08/09/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialInstruction : NSObject <NSCoding>

@property (nonatomic, retain) NSString * details;

@end
