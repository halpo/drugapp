//
//  DrugDetailViewController.m
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "DrugDetailViewController.h"

@interface DrugDetailViewController ()

@end

@implementation DrugDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	self.title = self.drug.name;
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
	if (!savePressed) {
		[[NSManagedObjectContext MR_defaultContext] reset];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark tableView methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headingView = [[UIView alloc] initWithFrame:(CGRect) {0, 0, tableView.frame.size.width * 0.75, 50.0 }];
    headingView.backgroundColor = [UIColor clearColor];
	UILabel *title = [[UILabel alloc] initWithFrame:CGRectOffset(headingView.frame, 10, 0)];
    title.backgroundColor = [UIColor clearColor];
	title.textColor = [UIColor darkGrayColor];
	title.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:20.0];
	switch ((int)section) {
		case 0:
			title.text = @"Drug Details";
			break;

		case 1:
			title.text = @"Side Effects";
			break;

		case 2:
			title.text = @"Special Instructions";
			break;

		default:
			break;
	}
	[headingView addSubview:title];
    if ((int)section == 1 || (int)section == 2) {
		UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
		addButton.frame = (CGRect) {tableView.frame.size.width - 40, 12, 25, 25 };
		[addButton setBackgroundImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
		(int)section == 1 ? [addButton addTarget:self action:@selector(addSideEffect) forControlEvents:UIControlEventTouchUpInside] : [addButton addTarget:self action:@selector(addSpecialInstruction) forControlEvents:UIControlEventTouchUpInside];
		[headingView addSubview:addButton];
	}
	return headingView;
}

- (void)addSideEffect {
	[self.view endEditing:YES];
	UIAlertView *sideEffectAlert = [[UIAlertView alloc] initWithTitle:@"New Side Effect" message:@"Enter details" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
	sideEffectAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
	sideEffectAlert.tag = 1;
	[sideEffectAlert show];
}

- (void)addSpecialInstruction {
	[self.view endEditing:YES];
	UIAlertView *specialInstructionAlert = [[UIAlertView alloc] initWithTitle:@"New Special Instruction" message:@"Enter details" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
	specialInstructionAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
	specialInstructionAlert.tag = 2;
	[specialInstructionAlert show];
}

#pragma mark alertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView textFieldAtIndex:0].text.length) {
		if (alertView.tag == 1 && buttonIndex == 1) {
			SideEffect *newSideEffect = [SideEffect new];
			newSideEffect.details = [alertView textFieldAtIndex:0].text;
			if (!self.drug.sideEffects) {
				self.drug.sideEffects = [NSArray new];
			}
			NSMutableArray *ma = [self.drug.sideEffects mutableCopy];
			[ma addObject:newSideEffect];
			self.drug.sideEffects = [NSArray arrayWithArray:ma];
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
		else if (alertView.tag == 2 && buttonIndex == 1) {
			SpecialInstruction *newSpecialInstruction = [SpecialInstruction new];
			newSpecialInstruction.details = [alertView textFieldAtIndex:0].text;
			if (!self.drug.specialInstructions) {
				self.drug.specialInstructions = [NSArray new];
			}
			NSMutableArray *ma = [self.drug.specialInstructions mutableCopy];
			[ma addObject:newSpecialInstruction];
			self.drug.specialInstructions = [NSArray arrayWithArray:ma];
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	switch ((int)section) {
		case 0:
			return 7;
			break;

		case 1:
			return [self.drug.sideEffects count];
			break;

		case 2:
			return [self.drug.specialInstructions count];
			break;
            
        case 3:
            return 1;

		default:
			break;
	}
	return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        NSString *value;
        NSString *labelText;
        switch (indexPath.row) {
            case 0:
                value = [kAppDelegate drugClass:self.drug.drugClass];
                labelText = @"Class";
                break;
            case 1:
                value = self.drug.name;
                labelText = @"Name";
                break;
            case 2:
                value = self.drug.indications;
                labelText = @"Indications";
                break;
            case 3:
                value = self.drug.modeOfAction;
                labelText = @"Mode of Action";
                break;
            case 4:
                value = self.drug.modeOfElimination;
                labelText = @"Mode of Elimination";
                break;
            case 5:
                value = self.drug.typicalDose;
                labelText = @"Typical Dose";
                break;
            case 6:
                value = self.drug.specialNotes;
                labelText = @"Special Notes";
                break;
                
            default:
                break;
        }
        if (indexPath.row != 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"InputCell"];
			if (!cell) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InputCell"];
			}
            UITextField *tf = (UITextField *)[cell viewWithTag:200];
            tf.text = value;
            //tf.userInteractionEnabled = NO;
            tf.tag = (int)indexPath.row;
            UILabel *label = (UILabel *)[cell viewWithTag:100];
            label.text = labelText;
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
			if (!cell) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectionCell"];
			}
            UILabel *label = (UILabel *)[cell viewWithTag:100];
            label.text = value;
        }
    }
    else if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NormalCell"];
		}
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0];
		cell.textLabel.text = [(SideEffect *)self.drug.sideEffects[indexPath.row] details];
    }
    else if (indexPath.section == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NormalCell"];
		}
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0];
		cell.textLabel.text = [(SpecialInstruction *)self.drug.specialInstructions[indexPath.row] details];
    }
    else if (indexPath.section == 3) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SaveCell"];
		}
	}
    //cell.userInteractionEnabled = NO;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (indexPath.section == 0 && indexPath.row == 0) {
		if (!picker) {
			CGRect pickerFrame = (CGRect) {0, [[UIScreen mainScreen] bounds].size.height - 216, 320, 216 };
			picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
			picker.dataSource = self;
			picker.delegate = self;
			picker.showsSelectionIndicator = YES;
			picker.backgroundColor = [UIColor whiteColor];
			[self.view addSubview:picker];
			[picker reloadAllComponents];
		}
		picker.hidden = NO;
	}
	if (indexPath.section == 3 && indexPath.row == 0) {
		[self saveChanges];
	}
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	if ((int)indexPath.section == 1 || (int)indexPath.section == 2) {
		return YES;
	}
	else {
		return NO;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if ((int)indexPath.section == 1) {
		NSMutableArray *sideEffects = [self.drug.sideEffects mutableCopy];
		[sideEffects removeObjectAtIndex:indexPath.row];
		self.drug.sideEffects = [NSArray arrayWithArray:sideEffects];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
	}
	else if ((int)indexPath.section == 2) {
		NSMutableArray *specialInstructions = [self.drug.specialInstructions mutableCopy];
		[specialInstructions removeObjectAtIndex:indexPath.row];
		self.drug.specialInstructions = [NSArray arrayWithArray:specialInstructions];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	picker.hidden = YES;
	[self.view endEditing:YES];
}

- (void)saveChanges {
	if ([self isValid]) {
		self.drug.name = [[(UITextField *)[self.tableView viewWithTag:1] text] capitalizedString];
		self.drug.indications = [(UITextField *)[self.tableView viewWithTag:2] text];
		self.drug.modeOfAction = [(UITextField *)[self.tableView viewWithTag:3] text];
		self.drug.modeOfElimination = [(UITextField *)[self.tableView viewWithTag:4] text];
		self.drug.typicalDose = [(UITextField *)[self.tableView viewWithTag:5] text];
		self.drug.specialNotes = [(UITextField *)[self.tableView viewWithTag:6] text];
		self.drug.isCore = NO;
		self.drug.lastEdited = [NSDate date];
        savePressed = YES;
		[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (BOOL)isValid {
	NSMutableArray *validArray = [NSMutableArray new];
    
	//newDrug.drugClass ?: [validArray addObject:@"Class required"];
	[(UITextField *)[self.tableView viewWithTag:1] text].length ? : [validArray addObject:@"Name required"];
	[(UITextField *)[self.tableView viewWithTag:2] text].length ? : [validArray addObject:@"Indication(s) required"];
	[(UITextField *)[self.tableView viewWithTag:3] text].length ? : [validArray addObject:@"Mode of Action required"];
	[(UITextField *)[self.tableView viewWithTag:4] text].length ? : [validArray addObject:@"Mode of elimination required"];
	[(UITextField *)[self.tableView viewWithTag:5] text].length ? : [validArray addObject:@"Dose required"];
    
	!validArray.count ? : [[[UIAlertView alloc] initWithTitle:@"Required Fields:" message:[validArray componentsJoinedByString:@"\n"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
	return !validArray.count;
}

#pragma mark pickerview
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	// Handle the selection
	[self.drug setDrugClass:(int)row];
	picker.hidden = YES;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	NSUInteger numRows = 9;
	return numRows;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	NSString *title;
	title = [kAppDelegate drugClass:(int)row];
	return title;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	int sectionWidth = 320;
	return sectionWidth;
}

@end
