//
//  SideEffect.m
//  med
//
//  Created by Paul on 08/09/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "SideEffect.h"

@implementation SideEffect

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.details forKey:@"Details"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self.details = [aDecoder decodeObjectForKey:@"Details"];
    return self;
}

@end
