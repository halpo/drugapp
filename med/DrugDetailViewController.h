//
//  DrugDetailViewController.h
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrugDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
    UIPickerView *picker;
    BOOL savePressed;
}

@property (strong, nonatomic) Drug *drug;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
