//
//  AppDelegate.m
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Data"];
	//[Drug MR_truncateAll];
	if (![Drug MR_countOfEntities]) {
		[self initCoreDrugs];
	}

	return YES;
}

- (void)initCoreDrugs {
	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"drugs" ofType:@"xml"];
	NSDictionary *xmlDic = [NSDictionary dictionaryWithXMLFile:filePath];
	NSArray *ds = [xmlDic valueForKeyPath:@"drug"];

	for (NSDictionary *drugDict in ds) {
		Drug *newDrug = [Drug MR_createEntity];
		newDrug.drugClass = [drugDict[@"classID"] intValue];
		newDrug.name = drugDict[@"name"];
        newDrug.isCore = YES;
        newDrug.lastEdited = [NSDate date];
	}

	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)printXML {
	NSArray *da = [Drug MR_findAllSortedBy:@"drugClass" ascending:YES];
	NSMutableString *ms = [NSMutableString new];
	for (Drug *d in da) {
		NSString *s = [NSString stringWithFormat:@"<drug><classID>%d</classID><name>%@</name></drug>", d.drugClass, d.name];
		[ms appendString:s];
	}
	NSLog(@"<drugs>%@</drugs>", ms);
}

- (NSString *)drugClass:(int)index {
	switch (index) {
		case 0:
			return @"Cardiovascular";
			break;

		case 1:
			return @"Antibiotics";
			break;

		case 2:
			return @"Antiemetics";
			break;

		case 3:
			return @"Sedatives";
			break;

		case 4:
			return @"Respiratory";
			break;

		case 5:
			return @"Endocrine";
			break;

		case 6:
			return @"Gastrointestinal";
			break;

		case 7:
			return @"CNS";
			break;

		case 8:
			return @"Dependency / Overdose";
			break;

		default:
			return @"";
			break;
	}
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[MagicalRecord cleanUp];
}

@end
