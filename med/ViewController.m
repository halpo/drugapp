//
//  ViewController.m
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Drug Database";
	self.addButton.target = self;
	self.addButton.action = @selector(addButtonPressed);
	self.tableView.sectionHeaderHeight = 50.0;
	sections = 9;
	[self updateData];
}

- (void)viewDidAppear:(BOOL)animated {
	[self updateData];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)updateData {
	NSMutableArray *items = [NSMutableArray new];
	for (int i = 0; i < sections; i++) {
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"drugClass == %d", i];
		NSArray *sectionArray = [Drug MR_findAllSortedBy:@"name" ascending:YES withPredicate:predicate];
		[items addObject:sectionArray];
	}
	self.drugArray = [NSArray arrayWithArray:items];
	[self.tableView reloadData];
}

#pragma mark tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (!searching) {
		return sections;
	}
	else {
		return 1;
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (!searching) {
		return [(NSArray *)self.drugArray[section] count];
	}
	else {
		return self.filteredDrugArray.count;
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (!searching) {
		AppDelegate *delegate = kAppDelegate;
		return [[delegate drugClass:(int)section] capitalizedString];
	}
	else {
		return @"Results";
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell"];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainCell"];
	}

	Drug *drug;
	if (!searching) {
		drug = self.drugArray[indexPath.section][indexPath.row];
	}
	else {
		drug = self.filteredDrugArray[indexPath.row];
	}

	UIView *indicator = (UIView *)[cell viewWithTag:5];
	UILabel *label = (UILabel *)[cell viewWithTag:3];

	if (drug.isCore) {
		[indicator setBackgroundColor:[UIColor clearColor]];
	}
	else {
		[indicator setBackgroundColor:kMedColor];
	}

	label.text = drug.name;

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self performSegueWithIdentifier:@"DrugDetailSegue" sender:indexPath];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	Drug *drug;

	if (!searching) {
		drug = self.drugArray[indexPath.section][indexPath.row];
	}
	else {
		drug = self.filteredDrugArray[indexPath.row];
	}

	if (!drug.isCore) {
		return YES;
	}
	else {
		return NO;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	Drug *drug;
	if (!searching) {
		drug = self.drugArray[indexPath.section][indexPath.row];
	}
	else {
		drug = self.filteredDrugArray[indexPath.row];
	}

	[drug MR_deleteEntity];
	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

	[self updateData];
}

- (void)refresh {
	[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self.view endEditing:YES];
}

#pragma mark SearchBar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchText];
	self.filteredDrugArray = [Drug MR_findAllSortedBy:@"name" ascending:YES withPredicate:predicate];
	if (searchText.length > 0) {
		searching = YES;
	}
	else {
		searching = NO;
	}
	[self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([[segue identifier] isEqualToString:@"DrugDetailSegue"]) {
		DrugDetailViewController *vc = (DrugDetailViewController *)[segue destinationViewController];
		NSIndexPath *indexPath = (NSIndexPath *)sender;
		Drug *drug;
		if (!searching) {
			drug = self.drugArray[indexPath.section][indexPath.row];
		}
		else {
			drug = self.filteredDrugArray[indexPath.row];
		}
		vc.drug = drug;
	}
}

- (void)addButtonPressed {
	[self performSegueWithIdentifier:@"AddDrugSegue" sender:nil];
}

- (void)showHud:(BOOL)show withMessage:(NSString *)message {
	if (!hud) {
		hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	}
	hud.labelText = message ? : @"Loading...";
	[hud show:show];
}

@end
