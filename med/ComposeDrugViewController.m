//
//  ComposeDrugViewController.m
//  med
//
//  Created by Paul on 16/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ComposeDrugViewController.h"

@interface ComposeDrugViewController ()

@end

@implementation ComposeDrugViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	self.title = @"New Drug";

	if (!newDrug) {
		savePressed = NO;
		newDrug = [Drug MR_createEntity];
	}
	self.tableView.clipsToBounds = YES;
	inputTypes = @[@1, @0, @0, @0, @0, @0, @0];
	inputNames = @[@"Class", @"Name", @"Indication(s)", @"Mode of Action", @"Mode of Elimination", @"Typical Dose", @"Special Notes"];
}

- (void)viewWillDisappear:(BOOL)animated {
	if (!savePressed) {
		[newDrug MR_deleteEntity];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (BOOL)isValid {
	NSMutableArray *validArray = [NSMutableArray new];

	//newDrug.drugClass ?: [validArray addObject:@"Class required"];
	[(UITextField *)[self.tableView viewWithTag:1] text].length ? : [validArray addObject:@"Name required"];
	[(UITextField *)[self.tableView viewWithTag:2] text].length ? : [validArray addObject:@"Indication(s) required"];
	[(UITextField *)[self.tableView viewWithTag:3] text].length ? : [validArray addObject:@"Mode of Action required"];
	[(UITextField *)[self.tableView viewWithTag:4] text].length ? : [validArray addObject:@"Mode of elimination required"];
	[(UITextField *)[self.tableView viewWithTag:5] text].length ? : [validArray addObject:@"Dose required"];

	!validArray.count ? : [[[UIAlertView alloc] initWithTitle:@"Required Fields:" message:[validArray componentsJoinedByString:@"\n"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

	return !validArray.count;
}

- (void)saveNewDrug {
	if ([self isValid]) {
		newDrug.name = [[(UITextField *)[self.tableView viewWithTag:1] text] capitalizedString];
		newDrug.indications = [(UITextField *)[self.tableView viewWithTag:2] text];
		newDrug.modeOfAction = [(UITextField *)[self.tableView viewWithTag:3] text];
		newDrug.modeOfElimination = [(UITextField *)[self.tableView viewWithTag:4] text];
		newDrug.typicalDose = [(UITextField *)[self.tableView viewWithTag:5] text];
		newDrug.specialNotes = [(UITextField *)[self.tableView viewWithTag:6] text];
		newDrug.isCore = NO;
		newDrug.lastEdited = [NSDate date];
		[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
		savePressed = YES;
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (IBAction)bgTouch {
	[self.view endEditing:YES];
}

- (void)addSideEffect {
    [self.view endEditing:YES];
	UIAlertView *sideEffectAlert = [[UIAlertView alloc] initWithTitle:@"New Side Effect" message:@"Enter details" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
	sideEffectAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
	sideEffectAlert.tag = 1;
	[sideEffectAlert show];
}

- (void)addSpecialInstruction {
    [self.view endEditing:YES];
	UIAlertView *specialInstructionAlert = [[UIAlertView alloc] initWithTitle:@"New Special Instruction" message:@"Enter details" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
	specialInstructionAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
	specialInstructionAlert.tag = 2;
	[specialInstructionAlert show];
}

#pragma mark alertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if ([alertView textFieldAtIndex:0].text.length) {
		if (alertView.tag == 1 && buttonIndex == 1) {
			SideEffect *newSideEffect = [SideEffect new];
			newSideEffect.details = [alertView textFieldAtIndex:0].text;
			if (!newDrug.sideEffects) {
				newDrug.sideEffects = [NSArray new];
			}
			NSMutableArray *ma = [newDrug.sideEffects mutableCopy];
			[ma addObject:newSideEffect];
			newDrug.sideEffects = [NSArray arrayWithArray:ma];
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
		else if (alertView.tag == 2 && buttonIndex == 1) {
			SpecialInstruction *newSpecialInstruction = [SpecialInstruction new];
			newSpecialInstruction.details = [alertView textFieldAtIndex:0].text;
			if (!newDrug.specialInstructions) {
				newDrug.specialInstructions = [NSArray new];
			}
			NSMutableArray *ma = [newDrug.specialInstructions mutableCopy];
			[ma addObject:newSpecialInstruction];
			newDrug.specialInstructions = [NSArray arrayWithArray:ma];
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
	}
}

#pragma mark tableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headingView = [[UIView alloc] initWithFrame:(CGRect) {0, 0, tableView.frame.size.width * 0.75, 50.0 }];
    headingView.backgroundColor = [UIColor clearColor];
	UILabel *title = [[UILabel alloc] initWithFrame:CGRectOffset(headingView.frame, 10, 0)];
    title.backgroundColor = [UIColor clearColor];
	title.textColor = [UIColor darkGrayColor];
	title.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:20.0];
	switch ((int)section) {
		case 0:
			title.text = @"Drug Details";
			break;

		case 1:
			title.text = @"Side Effects";
			break;

		case 2:
			title.text = @"Special Instructions";
			break;

		default:
			break;
	}
	[headingView addSubview:title];
	if ((int)section == 1 || (int)section == 2) {
		UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
		addButton.frame = (CGRect) {tableView.frame.size.width - 40, 12, 25, 25 };
		[addButton setBackgroundImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
		(int)section == 1 ? [addButton addTarget:self action:@selector(addSideEffect) forControlEvents:UIControlEventTouchUpInside] : [addButton addTarget:self action:@selector(addSpecialInstruction) forControlEvents:UIControlEventTouchUpInside];
		[headingView addSubview:addButton];
	}
	return headingView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	switch ((int)section) {
		case 0:
			return 7;
			break;

		case 1:
			return [newDrug.sideEffects count];
			break;

		case 2:
			return [newDrug.specialInstructions count];
			break;

		case 3:
			return 1;
			break;

		default:
			break;
	}
	return 0;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([inputTypes[indexPath.row] intValue] == 0 && indexPath.section == 0) {
		return nil;
	}
	else {
		return indexPath;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (indexPath.section == 0 && indexPath.row == 0) {
		if (!picker) {
			CGRect pickerFrame = (CGRect) {0, [[UIScreen mainScreen] bounds].size.height - 216, 320, 216 };
			picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
			picker.dataSource = self;
			picker.delegate = self;
			picker.showsSelectionIndicator = YES;
			picker.backgroundColor = [UIColor whiteColor];
			[self.view addSubview:picker];
			[picker reloadAllComponents];
		}
		picker.hidden = NO;
	}
	if (indexPath.section == 3 && indexPath.row == 0) {
		[self saveNewDrug];
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;

	if (indexPath.section == 0) {
		if ([inputTypes[indexPath.row] intValue] == 0) {
			cell = [tableView dequeueReusableCellWithIdentifier:@"InputCell"];
			if (!cell) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InputCell"];
			}
			UITextField *tf = (UITextField *)[cell viewWithTag:200];
			tf.tag = (int)indexPath.row;
			UILabel *label = (UILabel *)[cell viewWithTag:100];
			label.text = inputNames[indexPath.row];
		}
		else {
			cell = [tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
			if (!cell) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectionCell"];
			}
			UILabel *label = (UILabel *)[cell viewWithTag:100];
            if (selectedClass) {
                label.text = [kAppDelegate drugClass:newDrug.drugClass];
            } else {
                label.text = @"Select Class...";
            }
		}
	}
	else if (indexPath.section == 1) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NormalCell"];
		}
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0];
		cell.textLabel.text = [(SideEffect *)newDrug.sideEffects[indexPath.row] details];
	}
	else if (indexPath.section == 2) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"NormalCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NormalCell"];
		}
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17.0];
		cell.textLabel.text = [(SpecialInstruction *)newDrug.specialInstructions[indexPath.row] details];
	}
	else if (indexPath.section == 3) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"SaveCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SaveCell"];
		}
	}
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	if ((int)indexPath.section == 1 || (int)indexPath.section == 2) {
		return YES;
	}
	else {
		return NO;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if ((int)indexPath.section == 1) {
		NSMutableArray *sideEffects = [newDrug.sideEffects mutableCopy];
		[sideEffects removeObjectAtIndex:indexPath.row];
		newDrug.sideEffects = [NSArray arrayWithArray:sideEffects];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
	}
	else if ((int)indexPath.section == 2) {
		NSMutableArray *specialInstructions = [newDrug.specialInstructions mutableCopy];
		[specialInstructions removeObjectAtIndex:indexPath.row];
		newDrug.specialInstructions = [NSArray arrayWithArray:specialInstructions];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	picker.hidden = YES;
	[self.view endEditing:YES];
}

#pragma mark pickerview
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	[newDrug setDrugClass:(int)row];
	picker.hidden = YES;
    selectedClass = YES;
	[self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	NSUInteger numRows = 9;
	return numRows;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	NSString *title;
	title = [kAppDelegate drugClass:(int)row];
	return title;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	int sectionWidth = 320;
	return sectionWidth;
}

@end
