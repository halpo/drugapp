//
//  ComposeDrugViewController.h
//  med
//
//  Created by Paul on 16/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComposeDrugViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {
	NSArray *inputTypes;
	NSArray *inputNames;
	Drug *newDrug;
	BOOL savePressed;
    BOOL selectedClass;
	UIPickerView *picker;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)bgTouch;
@end
