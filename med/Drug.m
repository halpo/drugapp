//
//  Drug.m
//  med
//
//  Created by Paul on 08/09/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "Drug.h"


@implementation Drug

@dynamic drugClass;
@dynamic indications;
@dynamic isCore;
@dynamic lastEdited;
@dynamic modeOfAction;
@dynamic modeOfElimination;
@dynamic name;
@dynamic specialNotes;
@dynamic typicalDose;
@dynamic specialInstructions;
@dynamic sideEffects;

@end
