//
//  AppDelegate.h
//  med
//
//  Created by Paul on 15/07/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (NSString *)drugClass:(int)index;

@end
